package com.springboot.api.gradle.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.api.gradle.model.Empresa;

public class EmpresaRowMapper implements RowMapper<Empresa>{

	@Override
	public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
		Empresa empresa = new Empresa();
		
		empresa.setIdEmpresa(rs.getInt("id_empresa"));
		empresa.setRuc(rs.getString("ruc"));
		empresa.setRazonSocial(rs.getString("razon_social"));
		empresa.setEstadoActual(rs.getString("estado_actual"));
		
		return empresa;
	}

}
