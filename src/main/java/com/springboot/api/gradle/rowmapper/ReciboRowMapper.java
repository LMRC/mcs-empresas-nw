package com.springboot.api.gradle.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.api.gradle.model.Recibo;

public class ReciboRowMapper implements RowMapper<Recibo>{

	@Override
	public Recibo mapRow(ResultSet rs, int rowNum) throws SQLException {
		Recibo recibo = new Recibo();
		
		recibo.setIdEmpresa(rs.getInt("id_empresa"));
		recibo.setNroRecibo(rs.getString("nro_recibo"));
		recibo.setMontoEmitido(rs.getDouble("monto_emitido"));
		recibo.setFgRetencion(rs.getInt("fg_retencion"));
		
		return recibo;
	}

}
