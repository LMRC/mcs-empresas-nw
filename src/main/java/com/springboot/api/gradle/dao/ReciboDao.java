package com.springboot.api.gradle.dao;

import java.util.List;

import com.springboot.api.gradle.model.Recibo;

public interface ReciboDao {
	
	List<Recibo> getAllRecibos() throws Exception;
	List<Recibo> getRecibos(Integer idEmpresa) throws Exception;
	void saveRecibo(Recibo recibo) throws Exception;
	void deleteRecibo(Integer idEmpresa) throws Exception;

}
