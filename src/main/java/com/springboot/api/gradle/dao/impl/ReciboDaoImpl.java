package com.springboot.api.gradle.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.gradle.dao.ReciboDao;
import com.springboot.api.gradle.model.Recibo;
import com.springboot.api.gradle.rowmapper.ReciboRowMapper;

@Repository
public class ReciboDaoImpl extends JdbcDaoSupport implements ReciboDao {

	public ReciboDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public List<Recibo> getAllRecibos() {
		logger.debug("::::: Mensaje de prueba :::::::");
		List<Recibo> listaRecibos = new ArrayList<Recibo>();
		
		String sql = " SELECT id_empresa, nro_recibo, monto_emitido, fg_retencion\n" + 
				" FROM microservicios.recibo";
		
		try {
			
			RowMapper<Recibo> reciboRow = new ReciboRowMapper();
			listaRecibos = getJdbcTemplate().query(sql, reciboRow);
			logger.debug("Se han listado "+listaRecibos.size()+" recibos");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
		return listaRecibos;
	}

	@Override
	public List<Recibo> getRecibos(Integer idEmpresa) {
		logger.debug("::::: Mensaje de prueba :::::::");
		Recibo empresa = new Recibo();	
		List<Recibo> listaRecibos = new ArrayList<Recibo>();
		
		String sql = " SELECT id_empresa, nro_recibo, monto_emitido, fg_retencion\n" + 
				" FROM microservicios.recibo where id_empresa='"+idEmpresa+"'";
				
		try {
			
			RowMapper<Recibo> reciboRow = new ReciboRowMapper();
			listaRecibos = getJdbcTemplate().query(sql, reciboRow);
			logger.debug("Se han listado "+listaRecibos.size()+" recibos de la empresa con id " + idEmpresa);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
		return listaRecibos;
	}

	@Override
	public void saveRecibo(Recibo recibo) {
		
		String sql = "insert into microservicios.recibo (id_empresa, nro_recibo, monto_emitido, fg_retencion) "  
				+ "values (?, ?, ?, ?);";
		
		Object[] params = { recibo.getIdEmpresa(), recibo.getNroRecibo(), recibo.getMontoEmitido(), recibo.getFgRetencion()};
		int[] tipos = { Types.INTEGER, Types.VARCHAR, Types.DOUBLE, Types.INTEGER};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado el recibo "+recibo.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

	}

	@Override
	public void deleteRecibo(Integer idEmpresa) {
		int regeliminados = 0;		
		String sql = " delete from microservicios.recibo where id_empresa ='"+idEmpresa+"'";		
		try {			
			regeliminados = getJdbcTemplate().update(sql);
			logger.debug("Se han eliminado "+regeliminados+" recibos con idEmpresa = "+idEmpresa);
		} catch (Exception e) {			
			logger.error(e.getMessage());
			throw e;
		}
	}

}
