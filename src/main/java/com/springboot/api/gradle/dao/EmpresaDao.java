package com.springboot.api.gradle.dao;

import java.util.List;

import com.springboot.api.gradle.model.Empresa;

public interface EmpresaDao {
	
	List<Empresa> getAllEmpresas() throws Exception;
	Empresa getEmpresa(Integer idEmpresa) throws Exception;
	void saveEmpresa(Empresa empresa) throws Exception;
	void deleteEmpresa(Integer idEmpresa) throws Exception;

}
