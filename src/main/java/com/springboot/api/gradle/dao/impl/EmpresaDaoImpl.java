package com.springboot.api.gradle.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.gradle.dao.EmpresaDao;
import com.springboot.api.gradle.model.Empresa;
import com.springboot.api.gradle.rowmapper.EmpresaRowMapper;

@Repository
public class EmpresaDaoImpl extends JdbcDaoSupport implements EmpresaDao {

	public EmpresaDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public List<Empresa> getAllEmpresas() throws Exception {
		logger.debug("::::: Mensaje de prueba :::::::");
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id_empresa, ruc, razon_social, estado_actual\n" + 
				" FROM microservicios.empresa";
		
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			logger.debug("Se han listado "+listaEmpresas.size()+" empresas");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
		return listaEmpresas;
	}

	@Override
	public Empresa getEmpresa(Integer idEmpresa) throws Exception {
		logger.debug("::::: Mensaje de prueba :::::::");
		Empresa empresa = new Empresa();	
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		
		String sql = " SELECT id_empresa, ruc, razon_social, estado_actual\n" + 
				" FROM microservicios.empresa where id_empresa='"+idEmpresa+"'";
				
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			
			empresa = listaEmpresas.get(0);
			
			logger.debug("Se ha traido a la empresa "+listaEmpresas.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
		return empresa;
	}

	@Override
	public void saveEmpresa(Empresa empresa) throws Exception {
		
		String sql = "insert into microservicios.empresa (ruc, razon_social, estado_actual) "  
				+ "values (?, ?, ?);";
		
		Object[] params = { empresa.getRuc(), empresa.getRazonSocial(), empresa.getEstadoActual()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado a la empresa "+empresa.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

	}

	@Override
	public void deleteEmpresa(Integer idEmpresa) throws Exception {
		int regeliminados = 0;		
		String sql = " delete from microservicios.empresa where id_empresa ='"+idEmpresa+"'";		
		try {			
			regeliminados = getJdbcTemplate().update(sql);
			logger.debug("Se han eliminado "+regeliminados+" empresa con id = "+idEmpresa);
		} catch (Exception e) {			
			logger.error(e.getMessage());
			throw e;
		}
	}

}
