package com.springboot.api.gradle.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.gradle.dao.impl.EmpresaDaoImpl;
import com.springboot.api.gradle.dao.impl.ReciboDaoImpl;
import com.springboot.api.gradle.model.Empresa;
import com.springboot.api.gradle.model.EmpresaRecibo;
import com.springboot.api.gradle.model.Recibo;
import com.springboot.api.gradle.service.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmpresaDaoImpl _empresaDao;
	
	@Autowired
	private ReciboDaoImpl _reciboDao;
	
	@Override
	public List<Empresa> getAllEmpresas() throws Exception {
		try {
			
			return _empresaDao.getAllEmpresas();
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
			
		}
		
		
	}

	@Override
	public EmpresaRecibo getEmpresa(Integer idEmpresa) throws Exception {
		
		try {
			
			Empresa empresa = _empresaDao.getEmpresa(idEmpresa);
			
			List<Recibo> lstRecibo = _reciboDao.getRecibos(idEmpresa);
			
			EmpresaRecibo empresaRecibo = new EmpresaRecibo();
			
			empresaRecibo.setIdEmpresa(empresa.getIdEmpresa());;
			empresaRecibo.setRuc(empresa.getRuc());
			empresaRecibo.setRazonSocial(empresa.getRazonSocial());
			empresaRecibo.setEstadoActual(empresa.getEstadoActual());
			
			empresaRecibo.setLstRecibo(lstRecibo);
			
			return empresaRecibo;
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
			
		}
		
	}

	@Override
	public void saveEmpresa(Empresa empresa) throws Exception {
		try {
			_empresaDao.saveEmpresa(empresa);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}	
	}

	@Override
	public void deleteEmpresa(Integer id) throws Exception {
		try {
			_empresaDao.deleteEmpresa(id);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
	}

}
