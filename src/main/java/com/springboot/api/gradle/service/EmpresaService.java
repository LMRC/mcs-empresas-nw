package com.springboot.api.gradle.service;

import java.util.List;

import com.springboot.api.gradle.model.Empresa;
import com.springboot.api.gradle.model.EmpresaRecibo;

public interface EmpresaService {
	
	List<Empresa> getAllEmpresas() throws Exception;
	EmpresaRecibo getEmpresa(Integer idEmpresa) throws Exception;
	void saveEmpresa(Empresa empresa) throws Exception;
	void deleteEmpresa(Integer idEmpresa) throws Exception;
	
}
