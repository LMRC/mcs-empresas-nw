package com.springboot.api.gradle.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.gradle.dao.impl.ReciboDaoImpl;
import com.springboot.api.gradle.model.Recibo;
import com.springboot.api.gradle.service.ReciboService;

@Service
public class ReciboServiceImpl implements ReciboService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ReciboDaoImpl _reciboDao;
	
	@Override
	public List<Recibo> getAllRecibos() throws Exception {
		
		try {
			return _reciboDao.getAllRecibos();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
	}

	@Override
	public List<Recibo> getRecibos(Integer idEmpresa) throws Exception {
		try {
			return _reciboDao.getRecibos(idEmpresa);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
	}

	@Override
	public void saveRecibo(Recibo recibo) throws Exception {
		try {
			_reciboDao.saveRecibo(recibo);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}	
	}

	@Override
	public void deleteRecibo(Integer id) throws Exception {
		try {
			_reciboDao.deleteRecibo(id);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
		
	}

}
