package com.springboot.api.gradle.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.gradle.model.Recibo;
import com.springboot.api.gradle.service.impl.ReciboServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/metodos/recibo")
@Api(description = "Api para el mantenimiento de datos de recibos")
public class ReciboController {
	
	@Autowired
	private ReciboServiceImpl _reciboService;
	
	@GetMapping(value = "/all", produces = "application/json")
	@ApiOperation("Listado general de todos los recibos")
	public Map<String, Object> getAllRecibos(){
		
		Map<String, Object> mapRespuesta = new HashMap<>();
		
		try {
			
			mapRespuesta.put("codigoServicio", "0000");
			mapRespuesta.put("recibos", _reciboService.getAllRecibos());
			
		} catch(Exception ex) {
			
			mapRespuesta.put("codigoServicio", "9999");
			mapRespuesta.put("descripcion", "Error al consultar los recibos");
			
			
		}
		
		return mapRespuesta;
		
	}
	
	@PostMapping(value = "/save", produces = "application/json")	
	@ApiOperation("Grabado de nuevo recibo")
	public Map<String, Object> saveRecibo(@RequestBody Recibo recibo){
		
		Map<String, Object> mapRespuesta = new HashMap<>();
		
		try {
			
			_reciboService.saveRecibo(recibo);
			
			mapRespuesta.put("codigoServicio", "0000");
			mapRespuesta.put("descripcion", "Recibo registrado con exito");
			
		} catch(Exception ex) {
			
			mapRespuesta.put("codigoServicio", "9999");
			mapRespuesta.put("descripcion", "Error al registrar los datos del recibo");
			
			
		}
		
		return mapRespuesta;
		
	}

}
