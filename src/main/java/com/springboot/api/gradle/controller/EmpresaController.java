package com.springboot.api.gradle.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.gradle.model.Empresa;
import com.springboot.api.gradle.service.impl.EmpresaServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/metodos/empresa")
@Api(description = "Api para el mantenimiento de datos de empresas")
public class EmpresaController {
	
	@Autowired
	private EmpresaServiceImpl _empresaService;
	
	@GetMapping(value = "/all", produces = "application/json")
	@ApiOperation("Listado general de todos las empresas")
	public Map<String, Object> getAllEmpresas(){
		
		Map<String, Object> mapRespuesta = new HashMap<>();
		
		try {
			
			mapRespuesta.put("codigoServicio", "0000");
			mapRespuesta.put("empresas", _empresaService.getAllEmpresas());
			
			return mapRespuesta;
			
		} catch(Exception ex) {
			
			mapRespuesta.put("codigoServicio", "9999");
			mapRespuesta.put("descripcion", "Error al consultar las empresas");
			
		}
		
		return mapRespuesta;
		
	}
	
	@GetMapping(value = "/get/{id}", produces = "application/json")	
	@ApiOperation("Obtencion particular de una empresa por id")
	public Map<String, Object> getEmpresa(@ApiParam("ID de la empresa <Integer>") @PathVariable ("id") Integer id){
		
		Map<String, Object> mapRespuesta = new HashMap<>();
		
		try {
			
			mapRespuesta.put("codigoServicio", "0000");
			mapRespuesta.put("empresa", _empresaService.getEmpresa(id));
			
		} catch(Exception ex) {
			
			mapRespuesta.put("codigoServicio", "9999");
			mapRespuesta.put("descripcion", "Error al obtener los datos de la empresa");
			
		}
		
		return mapRespuesta;
		
		
	}
	
	@PostMapping(value = "/save", produces = "application/json")	
	@ApiOperation("Grabado de nueva empresa")
	public Map<String, Object> saveEmpresa(@RequestBody Empresa empresa){
		
		Map<String, Object> mapRespuesta = new HashMap<>();
		
		try {
			
			_empresaService.saveEmpresa(empresa);
			
			mapRespuesta.put("codigoServicio", "0000");
			mapRespuesta.put("descripcion", "Empresa registrada con exito");
			
		} catch(Exception ex) {
			
			mapRespuesta.put("codigoServicio", "9999");
			mapRespuesta.put("descripcion", "Error al registrar los datos de la empresa");
			
		}
		
		return mapRespuesta;
		
	}	
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")	
	@ApiOperation("Eliminacion de empresa")
	public Map<String, Object> deleteEmpresa(@ApiParam("ID de la empresa <Integer>") @PathVariable ("id") Integer id){
		
		Map<String, Object> mapRespuesta = new HashMap<>();
		
		try {
			
			_empresaService.deleteEmpresa(id);
			
			mapRespuesta.put("codigoServicio", "0000");
			mapRespuesta.put("descripcion", "Empresa eliminada con exito");
			
		} catch(Exception ex) {
			
			mapRespuesta.put("codigoServicio", "9999");
			mapRespuesta.put("descripcion", "Error al eliminar la empresa");
			
		}
		
		return mapRespuesta;
		
	}	

}
