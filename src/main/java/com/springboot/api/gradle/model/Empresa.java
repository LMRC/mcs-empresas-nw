package com.springboot.api.gradle.model;

import java.util.List;

public class Empresa {
	
	private Integer idEmpresa;
	private String ruc;
	private String razonSocial;
	private String estadoActual;
		
	public Empresa() {
		
	}
	
	public Empresa(Integer idEmpresa, String ruc, String razonSocial, String estadoActual) {
	
		this.idEmpresa = idEmpresa;
		this.ruc = ruc;
		this.razonSocial = razonSocial;
		this.estadoActual = estadoActual;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getEstadoActual() {
		return estadoActual;
	}

	public void setEstadoActual(String estadoActual) {
		this.estadoActual = estadoActual;
	}

	@Override
	public String toString() {
		return "Empresa [idEmpresa=" + idEmpresa + ", ruc=" + ruc
				+ ", razonSocial=" + razonSocial + ", estadoActual=" + estadoActual + "]";
	}
	
}
