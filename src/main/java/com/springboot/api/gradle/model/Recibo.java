package com.springboot.api.gradle.model;

public class Recibo {
	
	private Integer idEmpresa;
	private String nroRecibo;
	private Double montoEmitido;
	private Integer fgRetencion;
		
	public Recibo() {
		
	}
	
	public Recibo(Integer idEmpresa, String nroRecibo, Double montoEmitido, Integer fgRetencion) {
	
		this.idEmpresa = idEmpresa;
		this.nroRecibo = nroRecibo;
		this.montoEmitido = montoEmitido;
		this.fgRetencion = fgRetencion;
	}

	public Integer getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNroRecibo() {
		return nroRecibo;
	}

	public void setNroRecibo(String nroRecibo) {
		this.nroRecibo = nroRecibo;
	}

	public Double getMontoEmitido() {
		return montoEmitido;
	}

	public void setMontoEmitido(Double montoEmitido) {
		this.montoEmitido = montoEmitido;
	}

	public Integer getFgRetencion() {
		return fgRetencion;
	}

	public void setFgRetencion(Integer fgRetencion) {
		this.fgRetencion = fgRetencion;
	}

	@Override
	public String toString() {
		return "Recibo [idEmpresa=" + idEmpresa + ", nroRecibo=" + nroRecibo
				+ ", montoEmitido=" + montoEmitido + ", fgRetencion=" + fgRetencion + "]";
	}
	
}
