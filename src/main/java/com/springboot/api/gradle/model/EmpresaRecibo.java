package com.springboot.api.gradle.model;

import java.util.List;

public class EmpresaRecibo extends Empresa {
	
	private List<Recibo> lstRecibo;

	public List<Recibo> getLstRecibo() {
		return lstRecibo;
	}

	public void setLstRecibo(List<Recibo> lstRecibo) {
		this.lstRecibo = lstRecibo;
	}

}
